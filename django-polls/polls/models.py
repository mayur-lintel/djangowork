from __future__ import unicode_literals

import datetime

from django.db import models
from django.utils import timezone


class Question(models.Model):
    question_text = models.CharField('Question:', max_length=200)
    pub_date = models.DateTimeField('publish date', default=datetime.datetime.now())

    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now
    was_published_recently.admin_order_field = 'question_text'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published Recently?'


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    FRESHMAN = 'cd'
    YEAR_IN_SCHOOL_CHOICES = (
        ('Audio', (
            ('vinyl', 'Vinyl'),
            ('cd', 'CD'),
        )
        ),
        ('Video', (
            ('vhs', 'VHS Tape'),
            ('dvd', 'DVD'),
        )
        ),
        ('unknown', 'Unknown'),
        )
    year_in_school = models.CharField(max_length=10,
                                      choices=YEAR_IN_SCHOOL_CHOICES,
                                      default=FRESHMAN)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text
