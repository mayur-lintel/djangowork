# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-22 13:11
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0009_auto_20160122_1303'),
    ]

    operations = [
        migrations.AlterField(
            model_name='choice',
            name='year_in_school',
            field=models.CharField(choices=[('Audio', (('vinyl', 'Vinyl'), ('cd', 'CD'))), ('Video', (('vhs', 'VHS Tape'), ('dvd', 'DVD'))), ('unknown', 'Unknown')], default='cd', max_length=2),
        ),
        migrations.AlterField(
            model_name='question',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 22, 13, 11, 41, 817297), verbose_name='publish date'),
        ),
    ]
