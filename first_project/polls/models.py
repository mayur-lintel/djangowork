from __future__ import unicode_literals

import datetime

from multiselectfield import MultiSelectField
from django.conf import settings
from django.db import models
from django.utils import timezone


class Question(models.Model):
    question_text = models.CharField('Question', max_length=200, )
    pub_date = models.DateTimeField('publish date', default=datetime.datetime.now(), blank=True)

    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now
    was_published_recently.admin_order_field = 'question_text'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published Recently?'


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    FRESHMAN = 'cd'
    YEAR_IN_SCHOOL_CHOICES = (
        ('Audio', (
            ('vinyl', 'Vinyl'),
            ('cd', 'CD'),
        )
        ),
        ('Video', (
            ('vhs', 'VHS Tape'),
            ('dvd', 'DVD'),
        )
        ),
        ('unknown', 'Unknown'),
        )
    year_in_school = models.CharField(max_length=10,
                                      choices=YEAR_IN_SCHOOL_CHOICES,
                                      default=FRESHMAN)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text


class UserDetail(models.Model):
    name = models.CharField(max_length=50, default=None)
    email = models.EmailField()
    created_date = models.DateTimeField(default=datetime.datetime.now())


class Setting(models.Model):

    title = models.CharField(max_length=50)
    days_list = (('sun', 'Sunday'), ('mon', 'Monday'), ('tue', 'Tuesday'), ('wed', 'Wednesday'), ('thu', 'Thursday'),
                 ('fri', 'Friday'), ('sat', 'Saturday'))
    days = MultiSelectField(choices=days_list, max_length=50)
    time = models.TimeField(default=datetime.datetime.now().time())


class MyModel(models.Model):
    upload = models.FileField(upload_to=settings.STATIC_URL + 'uploads/')

