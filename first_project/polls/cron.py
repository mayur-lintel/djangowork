from string import lower
from threading import Timer

import cronjobs
from django.core.mail import EmailMessage
import django_excel as excel
import pyexcel.ext.xls
from .models import *


@cronjobs.register()
def run_first_cron():
    print "In Cron Jobs"
    now = datetime.datetime.now()
    for st in Setting.objects.all():
        if lower(now.strftime("%a")) in st.days:
            print "correct"

            def send_backup():
                sender_list = []
                for ud in UserDetail.objects.all():
                    sender_list.append(ud.email)
                msg = EmailMessage('Subject', 'Message.', 'mayur@lintelindia.com', sender_list)
                # today = datetime.datetime.today()
                # file_name = today.strftime('Backup %Y-%m-%d')
                # excel.make_response_from_tables([Question, Choice], 'xls', file_name="Backup")
                # msg.attach_file('/tmp/Backup.xls')
                msg.send()
                print "Msg Sent"

            def get_sec(s):
                l = s.split(':')
                return float(l[0]) * 3600 + int(l[1]) * 60 + int(l[2])

            seconds = get_sec(str(st.time))
            print seconds

            t = Timer(seconds, send_backup)
            t.start()

        else:
            print "sorry"
