from string import join

from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from import_export import resources
# Register your models here.
from .models import *


class ChoiceInline(admin.TabularInline):
    model = Choice
    readonly_fields = ['question', 'year_in_school', 'choice_text', 'votes']
    can_delete = False
    ordering = ['-id']

    def has_add_permission(self, request):
        return False


class QuestionResource(resources.ModelResource):
    class Meta:
        model = Question


class QuestionAdmin(ImportExportModelAdmin):
    # fields = [('pub_date', 'question_text')]
    fieldsets = [
        (None, {
            'classes': ['wide'],
            'fields': [('question_text', 'pub_date')]
            })
    ]
    inlines = [ChoiceInline]

    list_display = ('question_text', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['question_text']
    resource_class = QuestionResource
    save_on_top = True


class ChoiceAdmin(admin.ModelAdmin):
    class Media:
        def __init__(self):
            pass

        from django.conf import settings
        static_url = getattr(settings, 'STATIC_URL', '/static/')
        js = ['http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js', static_url+'admin/js/test.js']


class UserDetailAdmin(admin.ModelAdmin):
    fields = ('name', 'email')
    list_display = ('name', 'email')


class SettingAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    list_display = ('title', 'active_days', 'time')
    fields = ['title', 'days', 'time']
    readonly_fields = ['title']

    def active_days(self, instance):
        lst = []
        for v in instance.days:
            lst.append(dict(instance.days_list)[v])
        return join(lst, ", ")


admin.site.register(Question, QuestionAdmin)
admin.site.register(Choice, ChoiceAdmin)
admin.site.register(MyModel)
admin.site.register(UserDetail, UserDetailAdmin)
admin.site.register(Setting, SettingAdmin)
