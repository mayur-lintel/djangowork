from django import forms
from django.forms import ModelForm, SelectDateWidget

from .models import *


class NameForm(forms.Form):
    subject = forms.CharField(max_length=100, widget=forms.NumberInput)
    message = forms.CharField(widget=forms.Textarea(attrs={'style': 'color: red'}))
    send_date = forms.DateField(widget=SelectDateWidget(empty_label=("Choose Year", "Choose Month", "Choose Day")))
    FAVORITE_COLORS_CHOICES = (
        ('blue', 'Blue'),
        ('green', 'Green'),
        ('black', 'Black'),
    )
    favorite_colors = forms.MultipleChoiceField(required=False, widget=forms.CheckboxSelectMultiple,
                                                choices=FAVORITE_COLORS_CHOICES)
    CHOICES = (('1', 'First',), ('2', 'Second',))
    choice_field = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICES)
    sender = forms.EmailField()
    cc_myself = forms.BooleanField(required=False, initial=True)


class QuestionForm(ModelForm):
    class Meta:
        model = Question
        fields = ['question_text', 'pub_date']

